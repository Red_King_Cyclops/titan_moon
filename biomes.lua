--minetest.clear_registered_ores()
minetest.clear_registered_biomes()
minetest.clear_registered_decorations()
--minetest.override_item("default:mossycobble", {tiles = {"default_ice.png"}})

--ice pebbles biome
	minetest.register_biome({
		name = "ice_pebbles_land",
		--node_dust = "default:gravel",
		node_top = "titan_moon:ice_pebbles",
		depth_top = 1,
		node_filler = "titan_moon:ice",
		depth_filler = 3,
		node_stone = "titan_moon:ice",
		node_water_top = "titan_moon:liquid_hydrocarbon_source",
		depth_water_top =1 ,
		node_water = "titan_moon:liquid_hydrocarbon_source",
		node_river_water = "titan_moon:liquid_hydrocarbon_source",
		y_min = -31000,
		y_max = 500,
		heat_point = 10,
		humidity_point = 0,
	})

--hydrocarbon dunes biome
	minetest.register_biome({
		name = "hydrocarbon_dunes",
		--node_dust = "default:gravel",
		node_top = "titan_moon:hydrocarbon_sand",
		depth_top = 5,
		node_filler = "titan_moon:ice",
		depth_filler = 3,
		node_stone = "titan_moon:ice",
		node_water_top = "titan_moon:liquid_hydrocarbon_source",
		depth_water_top =1 ,
		node_water = "titan_moon:liquid_hydrocarbon_source",
		node_river_water = "titan_moon:liquid_hydrocarbon_source",
		y_min = -31000,
		y_max = 500,
		heat_point = 20,
		humidity_point = 0,
	})
