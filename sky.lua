--Version 0.2

pos = {x=0, y=0, z=0}

local space = 1000 --value for space, change the value to however you like.

--The skybox for space, feel free to change it to however you like.

local titanskybox = {
"titan_sky_pos_y.png^[transform3",
"titan_sky_neg_y.png^[transform1",
"titan_sky_pos_z.png",
"titan_sky_neg_z.png",
"titan_sky_neg_x.png",
"titan_sky_pos_x.png",
}
local spaceskybox = {
"sky_pos_y.png^[transform3",
"sky_neg_y.png^[transform1",
"sky_pos_z.png",
"sky_neg_z.png",
"sky_neg_x.png",
"sky_pos_x.png",
}

local time = 0

minetest.register_globalstep(function(dtime)
time = time + dtime
if time > 1 then for _, player in ipairs(minetest.get_connected_players()) do
time = 0

local name = player:get_player_name()
local pos = player:getpos()

   --If the player has reached Space
   if minetest.get_player_by_name(name) and pos.y >= space then
   player:set_physics_override(1, 0.6, 0.2) -- speed, jump, gravity
   --player:set_physics_override(1, 0.4, 0.1) -- speed, jump, gravity [default]
   player:set_sky({r = 0, g = 0, b = 0}, "skybox", spaceskybox) -- Sets skybox
   --player:set_sky({}, "regular", {}) -- Sets skybox, in this case it sets the skybox to it's default setting if and only if the player's Y value is less than the value of space.
   --player:set_clouds({density = 0})
   	player:set_clouds({
		density = 0,
		--color = "#fff0f0e5",
		--ambient = "#000000",
		--height = 120,
		--thickness = 16,
		--speed = {x = 0, y = -2},
	})

   --If the player is on Titan
   elseif minetest.get_player_by_name(name) and pos.y < space then
   player:set_physics_override(1, 0.6, 0.2) -- speed, jump, gravity
   player:set_sky({r = 239, g = 194, b = 109}, "skybox", titanskybox) -- Sets skybox
   --player:set_clouds({density = 0.7})
   	player:set_clouds({
		density = 0.5,
		color = "#EFC26D", --"#c5b75f",
		ambient = "#000000", --"#c5b75f",
		height = 120,
		thickness = 30, --16,
		speed = {x = 20, y = 0}, --y used to equal -2, x used to equal 0
	})
      
      end
         end
            end
               end)

--[[
minetest.register_on_leaveplayer(function(player)
local name = player:get_player_name()
   
   if name then

   player:set_sky({}, "regular", {})

         end
            end)
]]

--or (minetest.get_timeofday() * 24000 < 6000 and minetest.get_timeofday() * 24000 >= 18000)
--and (minetest.get_timeofday() * 24000 >= 6000 and minetest.get_timeofday() * 24000 < 18000)
--https://dev.minetest.net/minetest.get_timeofday
--https://dev.minetest.net/time_of_day
