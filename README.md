![Picture of the whole of Titan](https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Titan_in_true_color.jpg/220px-Titan_in_true_color.jpg)
# Titan Moon

This mod overrides the default terrain and replaces it with Titan, a moon of Saturn. Arguably, Titan is the most interesting moon known. The player can explore hydrocarbon dunes, ice pebble lands, and hydrocarbon lakes, and watch the fast moving clouds, Saturn, and the stars. If the player goes above y=1000, the skybox changes so the atmosphere disappears.

A new liquid called "Liquid Hydrocarbon" is commonly found on Titan. It can be collect by buckets. "Alien crystals" line the liquid hydrocarbon lakes and rivers. In order to destroy an alien crystal, you must break the crystal and replace the hydrocarbon sand node beneath it with some other node.

Currently the player cannot survive on Titan (they can breath, but there are no trees or ores for them to mine) and there is no oxygen system like the one in the vacuum mod. I plan to make hydrocarbon sand a fuel and make ice-blocks meltable into water in a furnace. I also plan to make a realms version of this mod. Liquid hydrocarbon rain, cryovolcanoes, and a subsurface water ocean could also be interesting additions.

The code is licenced under LGPL 2.1 and the textures are licenced under CC-BY-SA. Textures have been taken from or modified from Minetest Saturn by Foghrye4, default by the Minetest development team, and farlands by D00Med. The skybox code is modified code taken from https://forum.minetest.net/viewtopic.php?f=9&t=13775&hilit=skybox by emperor_genshin. The crystal node registration is modified code taken from farlands by D00Med.

This mod depends on default and bucket.