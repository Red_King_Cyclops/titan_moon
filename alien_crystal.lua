minetest.register_node("titan_moon:alien_crystal", {
	description = "Titan Alien Crystal",
	--tiles = {{
	--	name = "mapgen_crystal_anim.png",
	--	animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 1.00},
	--}},
    tiles = {"titan_moon_alien_crystal.png"},
	inventory_image = "titan_moon_alien_crystal_item.png",
    --wield_image = "titan_moon_alien_crystal_item.png",
	use_texture_alpha = true,
	drawtype = "nodebox",
	paramtype = "light",
	paramtype2 = "facedir",
	--light_source = 7,
	node_box = {
		type = "fixed",
		fixed = {
			{-0.25, -0.5, -0.25, 0.25, 0.875, 0.25}, -- NodeBox1
			{-0.1875, -0.5, -0.1875, 0.1875, 1.25, 0.1875}, -- NodeBox2
			{-0.625, -0.5, -0.1875, -0.3125, 0.5, 0.125}, -- NodeBox3
			{0.0625, -0.5, -0.625, 0.375, 0.3125, -0.3125}, -- NodeBox5
			{0.3125, -0.5, 0.3125, 0.5625, 0.5, 0.5625}, -- NodeBox7
			{-0.5625, -0.5, -0.125, -0.375, 0.4375, 0.0625}, -- NodeBox8
			{0.125, -0.5, -0.5625, 0.3125, 0.25, -0.375}, -- NodeBox9
			{0.375, -0.5, 0.375, 0.5, 0.4375, 0.5}, -- NodeBox10
		}
	},
	groups = {cracky=2, dig_immediate=3, flammable=1},
	sounds = default.node_sound_glass_defaults()
})

minetest.register_abm({
    nodenames = {"titan_moon:hydrocarbon_sand"},
    neighbors = {"titan_moon:liquid_hydrocarbon_source"},
    interval = 2.0, -- Run every 2 seconds
    chance = 2, -- Select every 1 in 2 nodes
    action = function(pos, node, active_object_count, active_object_count_wider)
        local pos = {x = pos.x, y = pos.y + 1, z = pos.z}
        if pos.y == 2 then --minetest.get_node(pos) == "air" then
            minetest.set_node(pos, {name = "titan_moon:alien_crystal"})
        end
    end
})

