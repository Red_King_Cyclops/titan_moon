--Solids

minetest.register_node("titan_moon:ice_pebbles", {
    description = "Titan Icestone with Ice Pebbles",
    tiles = {"titan_moon_ice_pebbles.png", "titan_moon_ice.png", "titan_moon_ice.png", "titan_moon_ice.png", "titan_moon_ice.png", "titan_moon_ice.png"},
    --is_ground_content = true,
    groups = {cracky=3}, --{crumbly=1, falling_node = 1},
    sounds = default.node_sound_stone_defaults(), --default.node_sound_gravel_defaults()
})

minetest.register_node("titan_moon:ice", {
    description = "Titan Icestone",
    tiles = {"titan_moon_ice.png"},
    --is_ground_content = true,
    groups = {cracky=3},
    sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("titan_moon:hydrocarbon_sand", {
	description = "Hydrocarbon Sand",
	tiles = {"titan_moon_hydrocarbon_sand.png"},
	groups = {crumbly = 3, falling_node = 1, sand = 1},
	sounds = default.node_sound_sand_defaults(),
})

--Liquids

minetest.register_node("titan_moon:liquid_hydrocarbon_source", {
	description = "Liquid Hydrocarbon Source",
	drawtype = "liquid",
	tiles = {
		{
			name = "titan_moon_liquid_hydrocarbon_source_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
	},
	special_tiles = {
		-- New-style water source material (mostly unused)
		{
			name = "titan_moon_liquid_hydrocarbon_source_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
			backface_culling = false,
		},
	},
	alpha = 160,
	paramtype = "light",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "source",
	liquid_alternative_flowing = "titan_moon:liquid_hydrocarbon_flowing",
	liquid_alternative_source = "titan_moon:liquid_hydrocarbon_source",
	liquid_viscosity = 1,
	post_effect_color = {a = 103, r = 219, g = 167, b = 83}, --rgb(219,167,83)
	groups = {liquid = 3, puts_out_fire = 1, cools_lava = 1}, --removed "water = 3, "
	sounds = default.node_sound_water_defaults(),
})

minetest.register_node("titan_moon:liquid_hydrocarbon_flowing", {
	description = "Flowing Liquid Hydrocarbon",
	drawtype = "flowingliquid",
	tiles = {"titan_moon_liquid_hydrocarbon.png"},
	special_tiles = {
		{
			name = "titan_moon_liquid_hydrocarbon_flowing_animated.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.8,
			},
		},
		{
			name = "titan_moon_liquid_hydrocarbon_flowing_animated.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.8,
			},
		},
	},
	alpha = 160,
	paramtype = "light",
	paramtype2 = "flowingliquid",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "flowing",
	liquid_alternative_flowing = "titan_moon:liquid_hydrocarbon_flowing",
	liquid_alternative_source = "titan_moon:liquid_hydrocarbon_source",
	liquid_viscosity = 1,
	post_effect_color = {a = 103, r = 219, g = 167, b = 83},
	groups = {liquid = 3, puts_out_fire = 1,
		not_in_creative_inventory = 1, cools_lava = 1}, --removed "water = 3, "
	sounds = default.node_sound_water_defaults(),
})
